const html = require('choo/html');
const post = require('./components/post.js');

module.exports = (state, emit) => {
  const onsubmit = e => {
    e.preventDefault();
    const form = e.currentTarget;
    const content = form['post-text'].value;
    emit('createPost', content);
  };

  const addVote = i => {
    emit('addVote', i);
  };

  const removeVote = i => {
    emit('removeVote', i);
  };

  return html`
    <div class="container">
      <form id="post" onsubmit=${onsubmit}>
        <textarea id="post-text" name="post-text" type="text"
          cols="30"
          rows="5"
          required
          pattern=".{1,100}"
          title="Post must be between 1 and 100 characters long."
         ></textarea>
        <input type="submit" value="Post">
      </form>
      <div class="content">
        ${state.posts.map(
          (item, i) =>
            item.type === 'post' ? post(addVote, removeVote, item, i) : ''
        )}
      </div>
    </div>
    `;
};
