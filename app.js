const choo = require('choo');
const app = choo();

app.use(require('./src/controller.js'));
app.route('/', require('./src/view.js'));
app.mount('#app');
