const html = require('choo/html');

module.exports = (addVote, removeVote, data, index) => {
  const add = () => {
    addVote(index);
  };

  const remove = () => {
    removeVote(index);
  };

  const date = new Date(data.date).toLocaleString();
  return html`
    <div class="flex">
      <div class="left">
        <button class="upvote" onclick=${add}>+</button>
        <span class="votes">${data.votes} votes</span>
        <button class="downvote" onclick=${remove}>-</button>
      </div>
      <div class="right">
        <p class="date">${date}</p>
        <p class="post">${data.content}</p>
      </div>
    </div>
  `;
};
