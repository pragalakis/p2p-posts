# p2p-posts

p2p workshop app

![screenshot](screenshot.png)

## What's inside? 

* Choo - a frontend framework
* Browserify - a bundler
* Budo - a dev server
* level-browserify - LevelDB wrapper (Indexedb Database) 
* hyperlog - a graph data structure 
* webrtc-swarm - p2p connections using webrtc 
* signalhub - signalling server 

## How to start?

* Clone this repo
* Remove the `.git` folder
* `npm install`
* `npm start` 


## Data structure

The data log is immutable


### Posts diagram

```mermaid
graph TD;
    A(post) -->B(post)
    B -->C(post)
    C -->D(post)
```


### Votes diagram

```mermaid
graph TD;
    A(single post) -->|like|LA1(value: 1)
    A -->|vote|VA2(value: 1)
    A -->|vote|VA3(value: -1)
    A -->|vote|VA4(value: -1)
    A -->|vote|VA5(value: 1)
```

