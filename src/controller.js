const level = require('level-browserify');
const hyperlog = require('hyperlog');
const db = level('feed');
const log = hyperlog(db);

const webrtc = require('webrtc-swarm');
const signalhub = require('signalhub');
const swarm = webrtc(
  signalhub('feed', ['https://signalhub-jccqtwhdwc.now.sh/'])
);

const getId = () =>
  '_' +
  Math.random()
    .toString(36)
    .substr(2, 6);

module.exports = (state, emitter) => {
  //p2p connection and db replication
  swarm.on('peer', (peer, id) => {
    peer.pipe(log.replicate({ live: true })).pipe(peer);
  });

  // init state from db
  state.posts = [];
  log
    .createReadStream()
    .on('data', data => {
      const post = JSON.parse(data.value.toString());
      post.key = data.key;
      state.posts.push(post);

      // append votes to the state
      if (post.type === 'vote') {
        state.posts.forEach(item => {
          if (item.key == data.links[0]) {
            item.votes += Number(post.value);
          }
        });
      }
    })
    .on('end', () => {
      emitter.emit('render');
    });

  // add vote
  emitter.on('addVote', index => {
    log.createReadStream().on('data', node => {
      if (node.change == index + 1) {
        state.posts[index].votes++;
        log.add(
          [node.key],
          JSON.stringify({ type: 'vote', value: '1', id: getId() })
        );
        emitter.emit('render');
      }
    });
  });

  // remove vote
  emitter.on('removeVote', index => {
    log.createReadStream().on('data', node => {
      if (node.change == index + 1) {
        state.posts[index].votes--;
        log.add(
          [node.key],
          JSON.stringify({ type: 'vote', value: '-1', id: getId() })
        );
        emitter.emit('render');
      }
    });
  });

  // create post
  emitter.on('createPost', data => {
    let key = '';
    const post = {
      type: 'post',
      content: data,
      date: new Date(),
      votes: 0
    };

    log
      .createReadStream()
      .on('data', node => {
        // get the last post node
        if (JSON.parse(node.value).content != 'vote') {
          key = node.key;
        }
      })
      .on('end', () => {
        log.add(key, JSON.stringify(post), (err, node) => {
          if (err) console.log(err);
          post.key = node.key;
          state.posts.push(post);
          emitter.emit('render');
        });
      });
  });
};
